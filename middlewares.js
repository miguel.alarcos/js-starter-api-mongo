const jwt = require('jwt-simple');

module.exports = function middlewares(app) {
  app.use('/api/*', async (req, res, next) => {
    res.locals.version = '0.0.1';
    // res.locals.token = { user: 'miguel' };
    next();
  });

  app.use('/api/auth/*', (req, res, next) => {
    try {
      const token = req.headers.authorization;
      res.locals.token = jwt.decode(token, 'secret');
      next();
    } catch (error) {
      next(error);
    }
  });
};
