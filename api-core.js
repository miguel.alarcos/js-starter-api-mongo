const { ObjectID } = require('mongodb');

module.exports = {
  find: (app, path, col, handler) => {
    app.get(path, async (req, res, next) => {
      try {
        const { db } = req.app.locals;
        const { token } = res.locals;
        const g = handler({
          req, res, query: req.query, token,
        });
        const { query, options } = g.next().value;
        const docs = await db.collection(col).find(query, options).toArray();
        const result = g.next(docs).value;
        const ret = { version: res.locals.version, data: result };
        res.json(ret);
      } catch (error) {
        next(error);
      }
    });
  },
  findOne: (app, path, col, handler) => {
    // eslint-disable-next-line consistent-return
    app.get(path, async (req, res, next) => {
      try {
        const { db } = req.app.locals;
        const { token } = res.locals;
        const g = handler({ req, res, token });
        const options = g.next().value;
        // eslint-disable-next-line no-underscore-dangle
        const _id = new ObjectID(req.params._id);
        const doc = await db.collection(col).findOne({ _id }, options);
        if (doc === null) {
          return next(new Error('Document not found'));
        }
        const result = g.next(doc).value;
        const ret = { version: res.locals.version, data: result };
        res.json(ret);
      } catch (error) {
        next(error);
      }
    });
  },
  push: (app, path, col, field, handler) => {
    app.put(path, async (req, res, next) => {
      try {
        const { db } = req.app.locals;
        const { token } = res.locals;
        // eslint-disable-next-line no-underscore-dangle
        const _id = new ObjectID(req.params._id);
        let { body } = req;
        const doc = await db.collection(col).findOne({ _id });
        if (doc === null) {
          return next(new Error('Document not found'));
        }
        body = handler({
          req, res, data: body, oldDoc: doc, token,
        });
        //  if (doc.owner === token.user) {
        await db.collection(col).updateOne({ _id },
          { $push: { [field]: body } });
        const ret = { version: res.locals.version, data: body };
        //  } else {
        //  ret = { version: res.locals.version, data: {} };
        //  }
        res.json(ret);
      } catch (error) {
        next(error);
      }
    });
  },
  insertOne: (app, path, col, handler) => {
    app.post(path, async (req, res, next) => {
      try {
        const { db } = req.app.locals;
        const { token } = res.locals;
        let { body } = req;
        body = handler({
          req, res, data: body, token,
        });
        await db.collection(col).insertOne(body);
        const ret = { version: res.locals.version, data: body };
        res.json(ret);
      } catch (error) {
        next(error);
      }
    });
  },
  updateOne: (app, path, col, handler) => {
    // eslint-disable-next-line consistent-return
    app.put(path, async (req, res, next) => {
      try {
        const { db } = req.app.locals;
        const { token } = res.locals;
        // eslint-disable-next-line no-underscore-dangle
        const _id = new ObjectID(req.params._id);
        let { body } = req;
        const doc = await db.collection(col).findOne({ _id });
        if (doc === null) {
          return next(new Error('Document not found'));
        }
        body = handler({
          req, res, data: body, oldDoc: doc, token,
        });

        //  if (doc.owner === token.user) {
        await db.collection(col).updateOne({ _id },
          { $set: body });
        const ret = { version: res.locals.version, data: body };
        //  } else {
        //  ret = { version: res.locals.version, data: {} };
        // }
        res.json(ret);
      } catch (error) {
        next(error);
      }
    });
  },
  updateArray: (app, path, col, field, handler) => {
    // eslint-disable-next-line consistent-return
    app.put(path, async (req, res, next) => {
      try {
        const { db } = req.app.locals;
        const { token } = res.locals;
        // eslint-disable-next-line no-underscore-dangle
        const _id = new ObjectID(req.params._id);
        const subId = new ObjectID(req.params.subId);
        let { body } = req;
        const doc = await db.collection(col).findOne({ _id },
          { projection: { [field]: { $elemMatch: { _id: subId } } } });
        if (doc === null) {
          return next(new Error('Document not found'));
        }
        if (doc[field] === undefined) {
          return next(new Error('Subdocument not found'));
        }
        body = handler({
          req, res, data: body, rootDoc: doc, subDoc: doc[field][0], token,
        });
        //  let ret;
        // if (doc[field][0].owner === token.user) {
        const fieldID = `${field}._id`;
        const fieldID2 = `${field}.$`;
        await db.collection(col).updateOne({ _id, [fieldID]: subId },
          { $set: { [fieldID2]: body } });
        const ret = { version: res.locals.version, data: body };
        //  } else {
        //  ret = { version: res.locals.version, data: {} };
        // }
        res.json(ret);
      } catch (error) {
        next(error);
      }
    });
  },
};
