function auxPick(doc, attrs) {
  const ret = {};
  attrs.forEach((x) => {
    ret[x] = doc[x];
  });
  return ret;
}

module.exports = {
  pick: (doc, access, user, oldDoc) => {
    const aux = oldDoc || doc;
    let ret = {};
    // eslint-disable-next-line no-restricted-syntax
    for (const { target, keys } of access) {
      if (target === '*' && keys === '*') {
        ret = doc;
        break;
      } else if (target === '*') {
        ret = auxPick(doc, keys);
        break;
      } else if (aux[target] === user && keys === '*') {
        ret = doc;
        break;
      } else if (aux[target] === user) {
        ret = auxPick(doc, keys);
        break;
      }
    }
    return ret;
  },
};
