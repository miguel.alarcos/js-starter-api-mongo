docker-compose up --build

Con docker compose tenemos un entorno donde podemos programar nuestra API y cada vez que guardemos se lanzarán todos los tests (jest).

Además se levanta la API (express + mongodb), lo que me permite hacer testing sin tener que hacer mock de la base de datos mongo, pues hay una instancia de ésta levantada.

Esto significa que en vez de hacer mock a una llamada a mongo, la llamada se hace realmente y podemos testear el valor devuelto que es lo que queremos hacer en verdad.
Imagina que haces un update con $inc. En lugar de testear que la llamada a mongo se hace con un {$inc: ...}, lo que se testea es el objeto final, con el valor incrementado.
Se trata de un end-2-end donde el test puede hacer uso directo de axios para hacer las llamadas a la API.

```javascript
const {
  updateOne, updateArray, insertOne, push, findOne, find,
} = require('./api-core');

const { pick } = require('./pick');

module.exports = function myRoutes(app) {
  find(app, '/api/auth/offer', 'test', function* gen({ query, token }) {
    const { color } = query;
    const data = yield { query: { color, owner: token.user }, options: {} };
    const access = [{ target: 'owner', keys: '*' }];
    return data.map(x => pick(x, access, token.user));
  });

  findOne(app, '/api/auth/offer/:_id', 'test', function* gen({ token }) {
    const doc = yield {
      projection: {
        owner: true, color: true, num: true, budgets: { $elemMatch: { budgeter: token.user } },
      },
    };
    if (doc.owner !== token.user) {
      throw new Error('Not owner of document');
    }
    const access = [{ target: 'owner', keys: '*' }];
    return pick(doc, access, token.user);
  });

  push(app, '/api/auth/offer/:_id/budget', 'test', 'budgets', ({
    req, res, data, oldDoc, token
  }) => {
    const access = [{ target: '*', keys: '*' }];
    const aux = pick(data, access);
    aux.owner = token.user;
    return aux;
  });

  updateOne(app, '/api/auth/offer/:_id', 'test', ({
    req, res, data, oldDoc, token
  }) => {
    if (oldDoc.owner !== token.user) {
      throw new Error('Not owner of document');
    }
    const access = [{ target: 'owner', keys: '*' }];
    //  throw new Error(JSON.stringify(data));
    return pick(data, access, token.user, oldDoc);
  });

  updateArray(app, '/api/auth/offer/:_id/budget/:subId', 'test', 'budgets', ({
    req, res, data, rootDoc, subDoc, token
  }) => {
    if (subDoc.owner !== token.user) {
      throw new Error('Not owner of document');
    }
    const access = [{ target: 'owner', keys: '*' }];
    return pick(data, access, token.user, subDoc);
  });

  insertOne(app, '/api/auth/offer', 'test', ({ req, res, data, token }) => {
    const access = [{ target: '*', keys: '*' }];
    const aux = pick(data, access);
    aux.owner = token.user;
    return aux;
  });
};
```
