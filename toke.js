const jwt = require('jwt-simple');

const payload = {
  user: 'miguel',
  roles: ['admin'],
};
const secret = 'secret';
const token = jwt.encode(payload, secret);
console.log(token);
const decoded = jwt.decode(token, secret);
console.log(decoded);
