const { MongoClient, ObjectID } = require('mongodb');
const axios = require('axios');
axios.defaults.adapter = require('axios/lib/adapters/http');

const url = 'mongodb://db:27017/test';
const dbName = 'test';

let db = null;
const client = new MongoClient(url);

const headers = { authorization: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibWlndWVsIiwicm9sZXMiOlsiYWRtaW4iXX0.ErNc40MdgPp5R9KT55CJ_KAXABrXXrdZuv7BSV-McaQ' };

describe('test update array', () => {
  // eslint-disable-next-line no-underscore-dangle
  let _id = null;
  let subId = null;

  beforeAll(async () => {
    await client.connect();
    db = client.db(dbName);
  });

  afterAll(() => {
    client.close();
  });

  beforeEach(async () => {
    _id = new ObjectID();
    subId = new ObjectID();
    await db.collection('test').insertOne({ _id, budgets: [{ _id: subId, owner: 'miguel', color: 'red' }] });
  });

  afterEach(async () => {
    await db.collection('test').deleteMany({ });
  });

  test('test simple update array one', async () => {
    expect.assertions(2);
    let doc = await axios.put(`http://web:8888/api/auth/offer/${_id}/budget/${subId}`, { color: 'blue' }, { headers });
    expect(doc.data).toEqual({ data: { color: 'blue' }, version: '0.0.1' });
    doc = await db.collection('test').findOne({ _id });
    expect(doc.budgets[0].color).toEqual('blue');
  });

  test('test update error', async () => {
    expect.assertions(1);
    const doc = await axios.put(`http://web:8888/api/auth/offer/000/budget/${subId}`, { color: 'blue' }, { headers });
    expect(doc.data).toEqual({ error: 'Argument passed in must be a single String of 12 bytes or a string of 24 hex characters' });
  });

  test('test update error document not found', async () => {
    expect.assertions(1);
    const doc = await axios.put(`http://web:8888/api/auth/offer/${subId}/budget/${_id}`, { color: 'blue' }, { headers });
    expect(doc.data).toEqual({ error: 'Document not found' });
  });

  test('test update error subdocument not found', async () => {
    expect.assertions(1);
    const doc = await axios.put(`http://web:8888/api/auth/offer/${_id}/budget/${_id}`, { color: 'blue' }, { headers });
    expect(doc.data).toEqual({ error: 'Subdocument not found' });
  });
});
