const { MongoClient, ObjectID } = require('mongodb');
const axios = require('axios');
axios.defaults.adapter = require('axios/lib/adapters/http');

const url = 'mongodb://db:27017/test';
const dbName = 'test';

let db = null;
const client = new MongoClient(url);

describe('test update array', () => {
  // eslint-disable-next-line no-underscore-dangle
  let _id = null;

  beforeAll(async () => {
    await client.connect();
    db = client.db(dbName);
  });

  afterAll(() => {
    client.close();
  });

  beforeEach(async () => {
    _id = new ObjectID();
    await db.collection('test').insertOne({
      _id, owner: 'miguel', color: 'red', num: 5,
    });
  });

  afterEach(async () => {
    await db.collection('test').deleteMany({ });
  });

  test('test simple updateOne', async () => {
    expect.assertions(2);
    let doc = await axios.put(`http://web:8888/api/auth/offer/${_id}`, { color: 'blue' },
      { headers: { authorization: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibWlndWVsIiwicm9sZXMiOlsiYWRtaW4iXX0.ErNc40MdgPp5R9KT55CJ_KAXABrXXrdZuv7BSV-McaQ' } });
    expect(doc.data).toEqual({ data: { color: 'blue' }, version: '0.0.1' });
    doc = await db.collection('test').findOne({ _id });
    // expect(doc.color).toEqual('blue');
    expect(doc).toEqual({_id, owner: 'miguel', color: 'blue', num: 5});
  });
});
