const { MongoClient, ObjectID } = require('mongodb');
const axios = require('axios');
axios.defaults.adapter = require('axios/lib/adapters/http');

const url = 'mongodb://db:27017/test';
const dbName = 'test';

let db = null;
const client = new MongoClient(url);

describe('test update array', () => {
  // eslint-disable-next-line no-underscore-dangle
  let _id = null;

  beforeAll(async () => {
    await client.connect();
    db = client.db(dbName);
  });

  afterAll(() => {
    client.close();
  });

  beforeEach(async () => {
    _id = new ObjectID();
    await db.collection('test').insertOne({
      _id, owner: 'miguel', color: 'red', num: 5, budgets: [{ budgeter: 'miguel', x: 0 }, { budgeter: 'bernardo', x: 1 }],
    });
  });

  afterEach(async () => {
    await db.collection('test').deleteMany({ });
  });

  test('test simple findOne', async () => {
    expect.assertions(1);
    const doc = await axios.get(`http://web:8888/api/auth/offer/${_id}`,
      { headers: { authorization: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibWlndWVsIiwicm9sZXMiOlsiYWRtaW4iXX0.ErNc40MdgPp5R9KT55CJ_KAXABrXXrdZuv7BSV-McaQ' } });
    expect(doc.data).toEqual({
      data: {
        _id: `${_id}`, owner: 'miguel', color: 'red', num: 5, budgets: [{ budgeter: 'miguel', x: 0 }],
      },
      version: '0.0.1',
    });
  });
});
