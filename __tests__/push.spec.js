const { MongoClient, ObjectID } = require('mongodb');
const axios = require('axios');
axios.defaults.adapter = require('axios/lib/adapters/http');

const url = 'mongodb://db:27017/test';
const dbName = 'test';

let db = null;
const client = new MongoClient(url);

const headers = { authorization: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibWlndWVsIiwicm9sZXMiOlsiYWRtaW4iXX0.ErNc40MdgPp5R9KT55CJ_KAXABrXXrdZuv7BSV-McaQ' };

describe('test update array', () => {
  // eslint-disable-next-line no-underscore-dangle
  let _id = null;

  beforeAll(async () => {
    await client.connect();
    db = client.db(dbName);
  });

  afterAll(() => {
    client.close();
  });

  beforeEach(async () => {
    _id = new ObjectID();
    await db.collection('test').insertOne({ _id, budgets: [] });
  });

  afterEach(async () => {
    await db.collection('test').deleteMany({ });
  });

  test('test simple update array one', async () => {
    expect.assertions(2);
    let doc = await axios.put(`http://web:8888/api/auth/offer/${_id}/budget`, { color: 'blue' }, { headers });
    expect(doc.data).toEqual({ data: { owner: 'miguel', color: 'blue' }, version: '0.0.1' });
    doc = await db.collection('test').findOne({ _id });
    expect(doc.budgets[0].color).toEqual('blue');
  });
});
