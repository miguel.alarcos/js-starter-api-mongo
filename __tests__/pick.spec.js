const { pick } = require('../pick');

describe('testing pick', () => {
  test('all all', () => {
    const doc = pick({ a: 1, b: 2 }, [{ target: '*', keys: '*' }]);
    expect(doc).toEqual({ a: 1, b: 2 });
  });

  test('all some', () => {
    const doc = pick({ a: 1, b: 2 }, [{ target: '*', keys: ['a'] }]);
    expect(doc).toEqual({ a: 1 });
  });

  test('user all', () => {
    const doc = pick({ a: 1, b: 2 }, [{ target: 'owner', keys: '*' }], 'miguel', { owner: 'miguel' });
    expect(doc).toEqual({ a: 1, b: 2 });
  });

  test('user some', () => {
    const doc = pick({ a: 1, b: 2 }, [{ target: 'owner', keys: ['a'] }], 'miguel', { owner: 'miguel' });
    expect(doc).toEqual({ a: 1 });
  });

  test('all after user', () => {
    const doc = pick({ a: 1, b: 2 }, [{ target: 'owner', keys: ['*'] }, { target: '*', keys: ['a'] }], 'vero', { owner: 'miguel' });
    expect(doc).toEqual({ a: 1 });
  });
});
