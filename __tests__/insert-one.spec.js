const { MongoClient, ObjectID } = require('mongodb');
const axios = require('axios');
axios.defaults.adapter = require('axios/lib/adapters/http');

const url = 'mongodb://db:27017/test';
const dbName = 'test';

let db = null;
const client = new MongoClient(url);

describe('test update array', () => {
  beforeAll(async () => {
    await client.connect();
    db = client.db(dbName);
  });

  afterAll(() => {
    client.close();
  });

  afterEach(async () => {
    await db.collection('test').deleteMany({ });
  });

  test('test simple insertOne', async () => {
    expect.assertions(2);
    let doc = await axios.post('http://web:8888/api/auth/offer', { color: 'orange' },
      { headers: { authorization: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoibWlndWVsIiwicm9sZXMiOlsiYWRtaW4iXX0.ErNc40MdgPp5R9KT55CJ_KAXABrXXrdZuv7BSV-McaQ' } });
    // eslint-disable-next-line no-underscore-dangle
    const _id = new ObjectID(doc.data.data._id);
    // eslint-disable-next-line no-underscore-dangle
    expect(doc.data).toEqual({ data: { _id: doc.data.data._id, color: 'orange', owner: 'miguel' }, version: '0.0.1' });
    doc = await db.collection('test').findOne({ _id });
    expect(doc).toEqual({ _id, color: 'orange', owner: 'miguel' });
  });
});
