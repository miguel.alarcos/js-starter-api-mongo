FROM node:11.6.0-alpine

WORKDIR /usr/app

RUN npm install -g nodemon
RUN npm install -g jest

COPY package.json .

RUN yarn

COPY . .
