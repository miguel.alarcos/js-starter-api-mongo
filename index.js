const express = require('express');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');
const middleware = require('./middlewares');
const routes = require('./routes');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

middleware(app);

async function main() {
  const url = 'mongodb://db:27017/test';
  const dbName = 'test';
  const client = new MongoClient(url);
  try {
    await client.connect();
    const db = client.db(dbName);
    app.locals.db = db;
    routes(app);
    app.use((error, req, res, next) => {
      res.json({ error: error.message });
    });
    //middleware(app);
    const server = app.listen(8888, () => {
      console.log('app running on port.', server.address().port);
    });
  } catch (err) {
    console.log(err.stack);
  } finally {
    // client.close();
  }
}

main();
